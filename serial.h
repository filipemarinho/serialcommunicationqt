#ifndef SERIAL_H
#define SERIAL_H

#include <QObject>
#include <QTimer>
#include <QSerialPort>
#include <QSerialPortInfo>

class Serial : public QObject
{
    Q_OBJECT
public:
    explicit Serial(QObject *parent = nullptr);
    ~Serial();
    struct Settings {
        QString name =  "ttyUSB0";
        qint32 baudRate = QSerialPort::Baud115200;
        QSerialPort::DataBits dataBits = QSerialPort::Data8;
        QSerialPort::Parity parity = QSerialPort::NoParity;
        QSerialPort::StopBits stopBits = QSerialPort::OneStop;
        QSerialPort::FlowControl flowControl = QSerialPort::NoFlowControl;
    };


signals:
    void readResponse();

private slots:
    void openSerialPort();
    void closeSerialPort();
    void writeData(const QByteArray &data);
    void readData();
    void handleError(QSerialPort::SerialPortError error);
    QSerialPortInfo findPortFromInfo();



private:
    Settings m_currentSettings;
    QByteArray m_readData;
    QSerialPort *m_serial = nullptr;
    QString name;
};

#endif // SERIAL_H
