import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.12

Window {
    width: 640
    height: 240
    visible: true
    title: qsTr("Serial Communication")

    RowLayout{
        anchors.fill: parent
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.margins: 5

        Layout.alignment: Qt.AlignHCenter

        Button {
            text: "Open Serial Port"
            onClicked: serial.openSerialPort()
        }
        Button {
            text: "Close Serial Port"
            onClicked: serial.closeSerialPort()
        }
        TextField {
            placeholderText: "Enter commands"
            selectByMouse: true
            onAccepted: {
                serial.writeData(text)
                text = ""
            }
        }
    }

    RowLayout{
        anchors.fill: parent.bottom
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.margins: 5

        Layout.alignment: Qt.AlignHCenter

        Button {
            text: "LED test"
            onClicked: serial.writeData("!")
        }

        Button {
            text: "Sleep"
            onClicked: serial.writeData("S")
        }


        TextField {
            placeholderText: "Enter Dioptria [-5,5]"
            selectByMouse: true
            onAccepted: {
                serial.writeData("D"+text)
            }
        }
    }

}
