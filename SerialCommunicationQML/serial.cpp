#include "serial.h"
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QDebug>
#include <QTimer>

Serial::Serial(QObject *parent):
    QObject(parent),
    m_serial(new QSerialPort(this))
{
    connect(m_serial, &QSerialPort::errorOccurred, this, &Serial::handleError);
//    Make the connect here data->serial
//    connect(m_console, &Serial::getData, this, &Serial::writeData);


    this->openSerialPort();

//    QTimer *timer = new QTimer(this);
//    connect(timer, &QTimer::timeout, this, [=] {
//        this->closeSerialPort();
//        delete this->m_serial;
//    });

//    QTimer *timer1 = new QTimer(this);
//    connect(timer1, &QTimer::timeout, this, [=] {
//        this->writeData("?");
//        this->writeData("!");
//    });
//    timer1->start(10000);
//    timer->start(60000);

}

Serial::~Serial()
{
    this->closeSerialPort();
    delete this->m_serial;
}


QSerialPortInfo Serial::findPortFromInfo()
{
    //search for the arduino port by manufacturer
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
//        qDebug() << "Name : " << info.portName();
//        qDebug() << "Manufacturer: " << info.manufacturer();
//        qDebug() << "Description : " << info.description();
        if (info.manufacturer() == "1a86") {
            m_currentSettings.name = info.portName();
            qDebug() << "Name : " << info.portName() << " find." <<endl;
            return info;

        } else {
            qDebug() << "No device found!" << endl;
        }

    }
    return {};
}

void Serial::openSerialPort()
{
    m_serial->setPort(findPortFromInfo());
    m_serial->setBaudRate(m_currentSettings.baudRate);
    m_serial->setDataBits(m_currentSettings.dataBits);

    if (m_serial->open(QIODevice::ReadWrite)) {
        qDebug() << "Serial: Connected";
        connect(m_serial, &QSerialPort::readyRead, this, &Serial::readData);

    } else {
        QString error = m_serial->errorString();
        qDebug() << "Serial: Open error " << error << endl;
    }

    m_serial->setParity(m_currentSettings.parity);
    m_serial->setStopBits(m_currentSettings.stopBits);
    m_serial->setFlowControl(m_currentSettings.flowControl);

}


void Serial::writeData(const QByteArray &data)
{
    qint64 b_written = m_serial->write(data);
    qDebug() << "Writing ... " << data << "lenght: " << b_written << endl;
    if (b_written != data.length()){
        qDebug() << "lenght written: " << b_written  << endl;
        QString error = m_serial->errorString();
        qDebug() << "Serial: Write error " << error << endl;
    }
}

void Serial::readData()
{
    const QByteArray data = m_serial->readLine();
    QString receivedData = QString::fromStdString(data.toStdString());
    qDebug() << "RX data :> " << receivedData <<  endl;
    emit readResponse();

}


void Serial::closeSerialPort()
{
    if (m_serial->isOpen())
        m_serial->close();
        qDebug() << "Serial: Disconnected" << endl;
}

void Serial::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {

        QString error1 = m_serial->errorString();
        qDebug() << "Serial:    Critical Error "<< error1 << ", closing..." << endl;
        closeSerialPort();
    }
}
